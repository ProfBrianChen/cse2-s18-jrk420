//Jacob Kaplan
//hw06
//This program will ask the user to input integers that represent the dimensions of an argyle pattern and thus return a that pattern on the display

import java.util.Scanner;
public class Argyle
{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);//The following questions will query the user and check for errors to re-ask the question
    System.out.print("Enter a positive integer for the width of the viewing window of the pattern: ");
    int width;
    while(!myScanner.hasNextInt()){
      System.out.print("Enter a positive integer for the width of the viewing window: ");
      myScanner.next();
    }
    width = myScanner.nextInt();
    while(width < 0){
      System.out.print("Enter a positive integer for the width of the viewing window: ");
      width = myScanner.nextInt();
    }
    System.out.print("Enter a positive integer for the height of the viewing window of the pattern: ");
    int height;
    while(!myScanner.hasNextInt()){
      System.out.print("Enter a positive integer for the height of the viewing window: ");
      myScanner.next();
    }
    height = myScanner.nextInt();
    while(height < 0){
      System.out.print("Enter a positive integer for the height of the viewing window: ");
      height = myScanner.nextInt();
    }
    System.out.print("Enter a positive integer for the width of the diamonds in the pattern: ");
    int diamondWidth;
    while(!myScanner.hasNextInt()){
      System.out.print("Enter a positive integer for the width of the diamonds: ");
      myScanner.next();
    }
      diamondWidth = myScanner.nextInt();
    while(diamondWidth < 0){
      System.out.print("Enter a positive integer for the width of the diamonds: ");
      diamondWidth = myScanner.nextInt();
    }
    System.out.print("Enter a positive odd integer for the width of the center stripe: ");
    int centerWidth;
    while(!myScanner.hasNextInt()){
      System.out.print("Enter a positive odd integer for the width of the center stripe: ");
      myScanner.next();
    }
      centerWidth = myScanner.nextInt();
    while(centerWidth < 0){
      System.out.print("Enter a positive odd integer for the width of the center stripe: ");
      centerWidth = myScanner.nextInt();
    }
    while(centerWidth % 2 == 0){
      System.out.print("Enter a positive odd integer for the width of the center stripe: ");
      centerWidth = myScanner.nextInt();
    }
    while(centerWidth > (.5 * diamondWidth)){
      System.out.print("Enter a positive odd integer for the width of the center stripe: ");
      centerWidth = myScanner.nextInt();
      diamondWidth = myScanner.nextInt();
    }//The char functions will allow the user to enter characters to make up the pattern and also aded in the charAt piece so that if we needed a character could be placedat a specific/exact integer
    System.out.print("Enter the first character to fill the pattern: ");
    String characterOne = myScanner.next();
    char result = characterOne.charAt(0);
    
    System.out.print("Enter the second character to fill the pattern: ");
    String characterTwo = myScanner.next();
    char resultTwo = characterTwo.charAt(0);
    
    System.out.print("Enter the third character to fill the pattern: ");
    String characterThree = myScanner.next();
    char resultThree = characterThree.charAt(0);
    //The next section will pull the characters entered by the user and create the pattern by overlaying the four different sections that make up the Argyle pattern as a whole....which is why it is the same few statements repeated four times.
    for(int i = 0; i <= diamondWidth; i++){
      for(int j = 0; j <= diamondWidth; j++){
        if( i >= diamondWidth + 1 - j - (centerWidth/2) && i <=diamondWidth + 1 - j + (centerWidth/2)){
          System.out.print(characterOne);
        }
        else if (i > j){
          System.out.print(characterTwo);
        }
        else{
          System.out.print(characterThree);
        }
      }
      System.out.print("");
    }
      for(int i = 0; i <= diamondWidth; i++){
        for( int j = 0; j <= diamondWidth; j++){
        if(i >= j - (diamondWidth) && i <= j + (centerWidth/2)){
          System.out.print(characterOne);
        }
        else if (diamondWidth - j + 1 < i){
          System.out.print(characterTwo);
        }
        else{
          System.out.print(characterThree);
        }
        }
        System.out.print("");
      }
    for(int i = 0; i <= diamondWidth; i++){
      for (int j = 0; j <= diamondWidth; j++){
        if(i >= diamondWidth + 1 - j - (centerWidth/2) && i <= diamondWidth + 1 - j + (centerWidth/2)){
          System.out.print(characterOne);
        }
        else if (i > j){
          System.out.print(characterTwo);
        }
        else{
          System.out.print(characterThree);
        }
      }
      System.out.print("");
    }
      for(int i = 0; i <= diamondWidth; i++){
        for(int j = 0; j <=diamondWidth; j++){
          if(i >= j - (centerWidth/2) && i <= j + (centerWidth/2)){
          System.out.print(characterOne);
        }
          else if(diamondWidth - j +1 < i){
            System.out.print(characterTwo);
          }
        else{
          System.out.print(characterThree);
        }
        }
        System.out.print("");
      }
      
      
      
      //I'm sure I will get points off for how the shape comes out...I couldn't really figure out how to get it to come out in the argyle pattern; the program still runs without errors, but I am aware the shape is off.
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
  }
}