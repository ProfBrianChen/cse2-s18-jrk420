import java.util.Scanner;

public class Area{
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in);
    String inputShape = "";
    
    while(!validShape(inputShape)){
      System.out.println("Please enter a valid shape: ");
      inputShape = scan.nextLine();
    }
    
    if (inputShape.equals("rectangle")){
      System.out.println("Please enter a valid length: ");
      if (!scan.hasNextDouble()){
        System.out.println("Please enter a valid length: ");
        scan.next();
      }
      double length = scan.nextDouble();
      
      System.out.println("Please enter a valid width: ");
      if(!scan.hasNextDouble()){
        System.out.println("Please enter a valid width: ");
        scan.next();
      }
      double width = scan.nextDouble();
      double areaRec = areaRectangle(length, width);
      System.out.println("The area of the rectangle is " + areaRec);
    }
    
    else if (inputShape.equals("triangle")){
      System.out.println("Please enter a valid base: ");
      if (!scan.hasNextDouble()){
        System.out.println("Please enter a valid base: ");
        scan.next();
      }
      double base = scan.nextDouble();
      
      System.out.println("Please enter a valid height: ");
      if(!scan.hasNextDouble()){
        System.out.println("Please enter a valid height: ");
        scan.next();
      }
      double height = scan.nextDouble();
      double areaTri = areaTriangle(base, height);
      System.out.println("The area of the triangle is " + areaTri);
    }
    
    else if (inputShape.equals("circle")){
      System.out.println("Please enter a valid radius: ");
      if (!scan.hasNextDouble()){
        System.out.println("Please enter a valid radius: ");
        scan.next();
      }
      double radius = scan.nextDouble();
      double areaCir = areaCircle(radius);
      System.out.println("The area of the Circle is " + areaCir);
    
    } 
  }
    
    public static boolean validShape(String input){
      if (input.equals("rectangle")){
        return true;
      }
      else if (input.equals("triangle")){
        return true;
      }
      else if (input.equals("circle")){
        return true;
      }
      else{
        System.out.println("This is an invalid shape. Please enter a valid shape!");
        return false;
      }
    }
    
    public static double areaRectangle(double length, double width){
      return length * width;
    }
    
    public static double areaTriangle(double base, double height){
      return 0.5 * base * height; 
    }
    
    public static double areaCircle(double radius){
      return Math.PI * radius * radius;
    }
    
    
    
  
}