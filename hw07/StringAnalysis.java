import java.util.Scanner;

public class StringAnalysis{
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in);

    
    System.out.println("Please enter a String: ");
    int value = 0;
    
    String input = scan.nextLine();
    System.out.println("If you want to look at all the characters enter 1 or if you want to look at only some of the characters enter 2");
    
    value = scan.nextInt();
    if (value != 1 && value !=2){
      System.out.println("Please input a valid number:");
      value = scan.nextInt();
    }
    
    if (value == 1){
      if (examine(input)){
        System.out.println("This string is all letters.");
      }
      else{
        System.out.println("This string is not all letters.");
      }
    }
    
    else if (value == 2){
      System.out.println("Please enter the number of character you would like to examine: ");
      if(!scan.hasNextInt()){
        System.out.println("Please enter a valid number: ");
        scan.next();
      }
      int number = scan.nextInt();
      if (examine(input, number)){
        System.out.println("This string is all letters.");
      }
      else{
        System.out.println("This string is not all letters.");
      }
      
    }
  }
  
  public static boolean examine(String input){
    boolean flag = true;
    for (int i = 0; i<input.length();i++){
      if (Character.isLetter(input.charAt(i)) == false){
        flag= false;
      }
    }
    return flag;
  }
  
  public static boolean examine(String input, int number){
    boolean flag = true;
    for (int i = 0; i<number;i++){
      if (Character.isLetter(input.charAt(i)) == false){
        flag= false;
      }
    }
    return flag;
  }
}