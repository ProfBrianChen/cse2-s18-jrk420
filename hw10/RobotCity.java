//Jacob Kaplan
//HW 10
//This program builds blocks of three digit numbers that represent a city in a window with a random height and width in the range of 10 to 15


import java.util.Arrays;
import java.lang.Math;
public class RobotCity{
  public static void main(String[] args){
    for(int i = 0; i< 5; i++){
      System.out.println(i+1);
      int [][] city = buildCity();
      display(city);
      System.out.println();
      int k = (int) (Math.random() * 10);
      city = invade(city, 5);
      display(city);
      System.out.println();
      city = update(city);
      display(city);
    }
  }
  //This method assigns the height and width of the city randomly within the range of 10-15
  public static int[][] buildCity(){    
    int width = 10 + (int) (Math.random() * 6);
    int height = 10 + (int) (Math.random() * 6);
    int[][]  city = new int[width][height];
    
    for(int r = 0; r<width; r++){
      for(int c = 0; c<height; c++){
        city[r][c] = 100 + (int) (Math.random()* 900);
        
      }
    }
   return city;
  }
  //This method makes sure that each column is 4 characters wide
  public static void display(int [][] city){
    for(int r = 0; r<city.length; r++){
      for(int c = 0; c < city[r].length; c++){
        System.out.printf("%4d", city[r][c]);
      }
      System.out.print( "\n");
    }
  }
  //This method assigns a random number of negative values to the orginally positive blocks
  public static int[][] invade(int[][] city, int k){
    for(int i = 0; i < k; i++){
      int r = (int)(Math.random() * city.length);
      int c = (int)(Math.random() * city[r].length);
    
      city[r][c] = city[r][c] * -1;
    } 
    
    return city;
  }
  //This method moves those random negatives over one row while not generating new negatives and eliminating those after the end of the random window size 10-15
  public static int[][] update(int[][] city){
    for(int r = 0; r<city.length; r++){
      for(int c = 0; c<city[r].length; c++){
        if(city[r][c] < 0){
          city[r][c] = city[r][c] * -1;
          if(c != city[r].length - 1){
            city[r][c+1] = city[r][c+1] * -1;
            c+=2;
          }
        }
      }
    }
    return city;
  }
}