Grade:  100/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
code compiles correctly
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Code runs properly and there are no RE
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
Overall, nice job on this assignment. It would have been nice to print out the care values and suits or at least a key instead of just numbers.s