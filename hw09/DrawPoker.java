//Jacob Kaplan


import java.util.Random;
import java.util.Arrays;
public class DrawPoker
{
  public static void main (String[] args)
  {
    int[] deck = new int[52];
    
    //creates deck with values 0-51
    for (int i = 0; i < 52; i++)
    {
      deck[i] = i;
    }
    
    //shuffles deck
    deck = shuffle(deck);
    
    int[] hand1 = new int[5];
    int[] hand2 = new int[5];
    
    //deals 5 cards to each player
    for (int i = 0; i < 5; i++)
    {
      hand1[i] = deck[i*2];
      hand2[i] = deck[(i*2)+1];
    }
    
    //prints out the hands [num(suit)]
    System.out.print("Hand1: ");
    for (int i = 0; i < 5; i++)
    {
      System.out.print(hand1[i]%13 + "(" + hand1[i]/13 + ") ");
    }
    System.out.println();
    
    System.out.print("Hand2: ");
    for (int i = 0; i < 5; i++)
    {
      System.out.print(hand2[i]%13 + "(" + hand2[i]/13 + ") ");
    }
    System.out.println();
    
    //if statements to assign point values to each hand to determine who won
    int player1 = 0;
    int player2 = 0;
    if (fullHouse(hand1))
    {
      player1 = 4;
    }
    else if (flush(hand1))
    {
      player1 = 3;
    }
    else if (three(hand1))
    {
      player1 = 2;
    }
    else if (pair(hand1))
    {
      player1 = 1;
    }
    
    if (fullHouse(hand2))
    {
      player2 = 4;
    }
    else if (flush(hand2))
    {
      player2 = 3;
    }
    else if (three(hand2))
    {
      player2 = 2;
    }
    else if (pair(hand2))
    {
      player2 = 1;
    }
    
    //if statements that check which player has the high card if neither have hand
    if (player1 == 0 && player2 == 0)
    {
      int high1 = hand1[0]%13;
      int high2 = hand2[0]%13;
      for (int i = 0; i < 5; i++)
      {
        if (hand1[i]%13 > high1)
        {
          high1 = hand1[i]%13;
        }
        if (hand2[i]%13 > high2)
        {
          high2 = hand2[i]%13;
        }
      }
      
      if (high1 > high2)
      {
        player1 = 1;
        player2 = 0;
      }
      else if (high2 > high1)
      {
        player1 = 0;
        player2 = 1;
      }
      else
      {
        player1 = 0;
        player2 = 0;
      }
    }
    
    //if statements that print out the result of which player won
    if (player1 > player2)
    {
      System.out.println("Player 1 Wins!!");
    }
    else if (player2 > player1)
    {
      System.out.println("Player 2 Wins!!");
    }
    else
    {
      System.out.println("Its a draw!!");
    }
  }
  
  //method to check if the player has a pair
  public static boolean pair(int[] hand)
  {
    //double for loop to check each index
    for (int x = 0; x < 5; x++)
    {
      for (int y = 0; y < 5; y++)
      {
        if (x != y)
        {
          if (hand[x]%13 == hand[y]%13)
          {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  //method to check if the player has 3 of a kind
  public static boolean three(int[] hand)
  {
    //triple for loop to go through each index
    for (int i = 0; i < 5; i++)
    {
      for (int j = 0; j < 5; j++)
      {
        for (int k = 0; k < 5; k++)
        {
          if (i != j && i != k && j != k)
          {
            if (hand[i]%13 == hand[j]%13 && hand[i]%13 == hand[k]%13 && hand[j]%13 == hand[k]%13)
            {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  //method to check if the player has a flush
  public static boolean flush(int[] hand)
  {
    int suit = hand[0]/13;
    if (hand[0]/13 == suit && hand[1]/13 == suit && hand[2]/13 == suit && hand[3]/13 == suit && hand[4]/13 == suit)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  //method to check if the player has a full house
  public static boolean fullHouse(int[] hand)
  {
    //finds the 2 values of cards 
    int val1 = hand[0]%13;
    int val2 = hand[0]%13;
    for (int i = 0; i < 5; i++)
    {
      if (val1 == val2)
      {
        val2 = hand[i]%13;
      }
      else
      {
        break;
      }
    }
    
    if (val1 == val2)
    {
      return false;
    }
    
    //counts how many cards are in the hand with those values
    int count1 = 0;
    int count2 = 0;
    for (int i = 0; i < 5; i++)
    {
      if (hand[i]%13 == val1)
      {
        count1++;
      }
      if (hand[i]%13 == val2)
      {
        count2++;
      }
    }
    
    //check if the counts are correct
    if ((count1 == 2 && count2 == 3) || (count1 == 3 && count2 == 2))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  //method to shuffle the deck
  public static int[] shuffle(int[] deck)
  {
    Random rand = new Random();
    for (int i = 0; i < 52; i ++)
    {
      int index = rand.nextInt(52);
      int temp = deck[i];
      deck[i] = deck[index];
      deck[index] = temp;
    }
    return deck;
  }
}










