//Jacob Kaplan
//Hw05
//This program will ask the user for information about a course they are taking and be able to re-ask the question if the user entered it in the worng form.


import java.util.Scanner;
public class Hw05
{
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    double CourseNumber;
    double NumberofTimesperWeek;
    double NumberofStudents;
   
    System.out.print("Enter the course number of the current course you are taking: ");
    while(!myScanner.hasNextDouble()) { //infinite loop to ask the question again
      System.out.println("Error...user has not entered a number...Enter the course number : ");//rewritten statement to ask the user to enter a number
      myScanner.next();
    }
    CourseNumber = myScanner.nextDouble();
    
    System.out.print("Enter the number of times this class meets per week: ");
    
    while(!myScanner.hasNextDouble()) {
      System.out.println("Error...user has not entered a number...Enter the number of times the class section meets per week: ");
       myScanner.next();
    }
    NumberofTimesperWeek = myScanner.nextDouble();
    
    System.out.print("Enter the number of students in the class: ");
    while(!myScanner.hasNextDouble()) {
      System.out.println("Error...user has not entered a number...Enter the number of students in this class: ");
      myScanner.next();
    }
    NumberofStudents = myScanner.nextDouble();
    
    System.out.print("Enter the name of the department this course is listed: ");
    String answer; 
    while(!myScanner.hasNext()) {
      System.out.println("Error...user has not entered a name...Enter the department name for this course: ");
      myScanner.next();
    }
    answer = myScanner.next();
    System.out.print("Enter the name of the professor teaching the course: ");
    String professor;
    while(!myScanner.hasNext()) {
       System.out.println("Error...user has not entered a name...Enter the name of the professor: ");
      myScanner.next();
    }
    professor = myScanner.next();
    
     System.out.print("Enter the time this class starts in words including if the course is in the morning or afternoon: ");//Asked for this value in words so that the user can say ten in the morning as opposed to only writing 10 if it were to ask for an int or double value
    String time; 
    while(!myScanner.hasNext()) {
       System.out.println("Error...user has not entered a time using words...Enter the class time: ");
      myScanner.next();
    }
    time = myScanner.next();
  }
}
    
   