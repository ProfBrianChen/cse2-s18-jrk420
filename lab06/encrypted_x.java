//Jacob Kaplan
//lab06 
//This program will ask the user to input an integer from 1-100 and return an x embedded in a rectangle of star characters


import java.util.Scanner;
public class encrypted_x
{
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter any integer between zero and one hundred: ");   //this will set up the dimensions of the rectangle
    int input;
    while(!myScanner.hasNextInt()){
      System.out.print("Enter any integer ranging from zero to one hundred: ");  //asking question again in case of incorrect inputs
      myScanner.next();
    }
    input = myScanner.nextInt();
    while(input < 0){
      System.out.print("Enter any integer ranging from zero to one hundred: ");  //asking question again in case of incorrect inputs
     input =myScanner.nextInt();
    }
    
    for(int i = 0; i < input; i++){
      for(int j = 0; j < input; j++){
        if(i==j || i+j==input-1){
          System.out.print(" ");
        }
        else{
          System.out.print("*");
        }
      }
      System.out.println();
    }
      
  }
}
 