//Jacob Kaplan
//Hw04
//cse 02
//This program replicates the game of Yahtzee by either using the numbers plugged in by the user or randomly generating numbers in order to be used a dice roll values.This

import java.lang.Math;
import java.util.Scanner;
public class Yahtzee 
{
public static void main(String[] args)
{ 
Scanner myScanner = new Scanner(System.in);
int firstRoll = 0;
int secondRoll = 0;
int thirdRoll = 0;
int fourthRoll = 0;
int fifthRoll = 0;
System.out.print("Roll the dice or input values of a roll? type 'roll' or 'input': ");
String answer = myScanner.nextLine();
if(answer.equals( "roll"))
{
firstRoll = (int)(Math.random()*5)+1;
secondRoll = (int)(Math.random()*5)+1;
thirdRoll = (int)(Math.random()*5)+1;
fourthRoll = (int)(Math.random()*5)+1;
fifthRoll = (int)(Math.random()*5)+1;

  //System.out.println(firstRoll + " " + secondRoll+ " "+thirdRoll+ " "+fourthRoll+" "+fifthRoll+ "word");
}
  else if(answer.equals("input"))
{
  System.out.print("Enter in a the first roll (a number from 1 to 6)");
  firstRoll = myScanner.nextInt();
  System.out.print("Enter a number for the second roll: ");
  secondRoll = myScanner.nextInt();
  System.out.print("Enter a number for the third roll: ");
  thirdRoll = myScanner.nextInt();
  System.out.print("Enter a number for the fourth roll: ");
  fourthRoll = myScanner.nextInt();
  System.out.print("Enter a number for the fifth roll: ");
  fifthRoll = myScanner.nextInt();
  
}
  
int Aces = 0;
int Twos = 0;
int Threes = 0;
int Fours = 0;
int Fives = 0;
int Sixes = 0;
  //The following section indicates the suit or value of each die
  switch (firstRoll){
    case 1:
      Aces++;
      break;
    case 2:
      Twos++;
    case 3:
      Threes++;
    case 4:
      Fours++;
    case 5:
      Fives++;
    case 6:
      Sixes++;
      
  }
  switch (secondRoll){
    case 1:
      Aces++;
      break;
    case 2:
      Twos++;
    case 3:
      Threes++;
    case 4:
      Fours++;
    case 5:
      Fives++;
    case 6:
      Sixes++;
  }
  switch (thirdRoll){
    case 1:
      Aces++;
      break;
    case 2:
      Twos++;
    case 3:
      Threes++;
    case 4:
      Fours++;
    case 5:
      Fives++;
    case 6:
      Sixes++;
  }
  switch (fourthRoll){
    case 1:
      Aces++;
      break;
    case 2:
      Twos++;
    case 3:
      Threes++;
    case 4:
      Fours++;
    case 5:
      Fives++;
    case 6:
      Sixes++;
  }
 switch (fifthRoll){
    case 1:
      Aces++;
      break;
    case 2:
      Twos++;
    case 3:
      Threes++;
    case 4:
      Fours++;
    case 5:
      Fives++;
    case 6:
      Sixes++;
 }
  
  int upperInitial = (Aces*1)+(Twos*2)+(Threes*3)+(Fours*4)+(Fives*5)+(Sixes*6);
  int upperSectionTotal = upperInitial;
  if (upperInitial >= 63) {
    upperSectionTotal = upperInitial+35;
  }
  //The following section factors in the bonuses included in the lower section of Yahtzee
  int lowerTotal = 0;
  if(Aces == 3){
    lowerTotal = 3;
  }
  if(Twos == 3){
    lowerTotal = 6;
  }
  if(Threes == 3){
    lowerTotal = 9;
  }
  if(Fours == 3){
    lowerTotal = 12;
  }
  if(Fives == 3){
    lowerTotal = 15;
  }
  if(Sixes == 3){
    lowerTotal = 18;
  }
  

  if(Aces == 4){
    lowerTotal = 4;
  }
  if(Twos == 4){
    lowerTotal = 8;
  }
  if(Threes == 4){
    lowerTotal = 12;
  }
  if(Fours == 4){
    lowerTotal = 16;
  }
  if(Fives == 4){
    lowerTotal = 20;
  }
  if(Sixes == 4){
    lowerTotal = 24;
  }
  
  
  if((Aces == 2 && Twos == 3) || (Aces == 2 && Threes == 3) || (Aces == 2 && Fours == 3) ||
     (Aces == 2 && Fives == 3) || (Aces == 2 && Sixes == 3) || (Aces == 3 && Twos == 2) ||
     (Aces == 3 && Threes == 2) || (Aces == 3 && Fours == 2) || (Aces == 3 && Fives == 2) ||
     (Aces == 3 && Sixes == 2)){
    lowerTotal = 25;
  }
  if((Twos == 2 && Threes == 3) || (Twos == 2 && Fours == 3) || (Twos == 2 && Fives == 3) || 
     (Twos == 2 && Sixes == 3) || (Twos == 3 && Threes == 2) || (Twos == 3 && Fours == 2) || 
     (Twos == 3 && Fives == 2)  || (Twos == 3 && Sixes == 2)){
    lowerTotal = 25;
  }
  if((Threes == 2 && Fours == 3) || (Threes == 2 && Fives == 3) || (Threes == 2 && Sixes == 3) ||
  (Threes == 3 && Fours == 2) || (Threes == 3 && Fives == 2) || (Threes == 3 && Sixes == 2)){
    lowerTotal = 25;
  }
  if((Fours == 2 && Fives == 3) || (Fours == 2 && Sixes == 3) || (Fours == 3 && Fives == 2) ||
  (Fours == 3 && Sixes == 2)) {
    lowerTotal = 25;
  }
  if((Fives == 2 && Sixes == 3) || (Fives == 3 && Sixes == 2)){
    lowerTotal = 25;
  }
  if((Aces == 1 && Twos == 1 && Threes == 1 && Fours == 1) || (Twos == 1 && Threes == 1 && Fours == 1 && Fives == 1)
  || (Threes == 1 && Fours == 1 && Fives == 1 && Sixes == 1)){
    lowerTotal = 30;
  }
  if((Aces == 1 && Twos == 1 && Threes == 1 && Fours == 1 && Fives == 1) ||
    (Twos == 1 && Threes == 1 && Fours == 1 && Fives == 1 && Sixes == 1)){
    lowerTotal = 40;
  }
  if(Aces == 1 && Twos == 1 && Threes == 1 && Fours == 1 && Fives == 1 && Sixes == 1){
    lowerTotal = 50;
  }
  int grandTotal = (upperSectionTotal + lowerTotal);
    System.out.println("Your upper section score: " + upperInitial);
    System.out.println("Your lower section score: " + lowerTotal );
    System.out.println("Your grand total: " + grandTotal);
  

   
    
  
    
  
        
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
}

}

