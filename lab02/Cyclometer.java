//
// Jacob Kaplan CSE 02
//Lab 02
//This program displays the distance in miles of two bike trips both individually and combined
//This program displays the duration of each trip in minutes and the number count of tire rotations for each trip
public class Cyclometer  {
  public static void main (String[] args)  {
    int secsTrip1=480;  //trip 1 time in seconds
    int secsTrip2=3220;  //trip 2 time in seconds
	  int countsTrip1=1561;  //trip 1 count of rotations
	  int countsTrip2=9037;  //trip 2 count of rotations
    double wheelDiameter=27.0,  //wheel diameter of the bike
    PI=3.14159,  //
    feetPerMile=5280,  //
    inchesPerFoot=12,  //
    secondsPerMinute=60;  //
    double distanceTrip1, distanceTrip2,totalDistance;  //trip 1 distance, trip 2 distance, total distance
    System.out.println("Trip 1 took "+(480/60)+" minutes and had "+1561+" counts.");
    System.out.println("Trip 2 took "+(3220/60)+" minutes and had "+9037+" counts.");
	  // distanceTrip1=132408.594 inches
			// 132408.594/63360=2.08978 miles
		//distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
			// 766544.818/63360= 12.09825 miles
		// totalDistance= 14.18803 miles
    System.out.println("Trip 1 was "+2.08978+"miles") ;
		System.out.println("Trip 2 was "+12.09825+"miles") ;
		System.out.println("The total distance was "+14.18803+"miles") ;
  }
}