//Jacob Kaplan 
//Hw03 
// This program will ask the user for the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average in order to convert the quantity of rain into cubic miles

import java.util.Scanner;
public class Convert{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the affected area in acres: ");
    double numAcres = myScanner.nextDouble();
    System.out.print("Enter, in inches, the amount of rain on average: ");
    double avgRain = myScanner.nextDouble();
    double squareMiles; //in order to convert acres to miles
    squareMiles = numAcres/640;
    double milesOfRain;
    milesOfRain = avgRain/63360; //63360 is the number of inches in a mile
    double cubicMiles;
    cubicMiles = milesOfRain*squareMiles;
    System.out.println(cubicMiles + " Cubic Miles of rain");
    
    
  }
}