//Jacob Kaplan
//hw03
//This program will ask the user for the dimensions of a pyramid and return the volume inside the pyramid

import java.util.Scanner;
public class Pyramid {
public static void main(String[] args) {
     Scanner myScanner = new Scanner(System.in);
     System.out.print("Enter one of the base lengths of the pyramid: ");
     double baseLength = myScanner.nextDouble();
     System.out.print("Enter the pyramid height: ");
     double pyrHeight = myScanner.nextDouble();
     double numerator; //the numerator in the formula for a pyramid
     numerator = (baseLength*baseLength)*pyrHeight;
     double answer; //this will end up being the final answer
     answer = numerator/3 ;
     System.out.println("The volume of the pyramid is " + answer + "units cubed");
     
  
     }
     }