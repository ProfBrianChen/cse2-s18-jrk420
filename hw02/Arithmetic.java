//Jacob Kaplan CSE 02
//Hw 02
public class Arithmetic {
  public static void main(String[] args) {
    int numPants = 3;  //number of pairs of pants
    double pantsPrice = 34.98;  //cost per pair of pants
    int numShirts = 2;  //number of sweatshirts
    double shirtPrice = 24.99;  //price per sweatshirt
    int numBelts = 1;  //number of belts
    double beltCost = 33.99; // cost per belt
    double paSalesTax = 0.06;  //the tax rate
    double totalCostOfPants;  //total cost of pants
    double totalCostOfShirts;  //total cost of shirts
    double totalCostOfBelts;  //total cost of belts
    double beltSalesTaxCharge;  //sales tax from belts
    double shirtSalesTaxCharge;  //sales tax from shirts
    double pantsSalesTaxCharge;  //sales tax from pants
    double totalPurchaseCost;  //total cost of all items before tax
    double totalSalesTaxCharge;  //total amount of sales tax charged
    double totalTransactionAmount;  //total amount of transaction tax included
    System.out.println("The total cost of "+1+" belt is "+(1*33.99)+" with a tax of "+(33.99*0.06));  //` beltCost*numBelts, paSalesTax*totalCostOfBelts=beltSalesTaxCharge
    System.out.println("The total cost of "+2+" shirts is "+(2*24.99)+" with a tax of "+(49.98*0.06));  //shirtPrice*numShirts, paSalesTax*totalCostOfShirts=shirtSalesTaxCharge
    System.out.println("The total cost of "+3+" pants is "+(3*34.98)+" with a tax of "+(104.94*0.06));  //pantsPrice*numPants, paSalesTax*totalCostOfPants=pantsSalesTaxCharge
    System.out.println("The total cost of all items purchased, before tax is "+(33.99+49.98+104.94));  //totalCostOfBelts+totalCostOfShirts+totalCostOfPants
    System.out.println("The total cost of all sales tax is "+(2.04+2.99+6.30));  //beltSalesTaxCharge+shirtSalesTaxCharge+pantsSalesTaxCharge=totalSalesTaxCharge
    System.out.println("The total cost of the transaction is "+(11.33+188.91));  //totalSalesTaxCharge+totalPurchaseCost=totalTransactionAmount
      
    
    
    
      
  }
}
